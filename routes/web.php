<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ActivityModelController;
use App\Http\Controllers\SubactivityController;
use App\Http\Controllers\DataController;
use App\Http\Controllers\DataMultipleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return view('auth.login');      
});


/*Route::get('/login',function(){
return view('auth.login');
})->middleware('verified')->name('login');*/


Route::get('/login',[LoginController::class,'index'])->name('login');
Route::post('/login',[LoginController::class,'store']);
Route::get('/logout',[LoginController::class,'logout'])->name('logout');

Route::get('/register',[RegisterController::class,'index'])->name('register');
Route::post('/register',[RegisterController::class,'store']);

Route::group(['middleware'=>'auth'],function(){ 

//prefix('activity-model')->name('activity.model.')->
Route::group(['prefix'=>'activity-model','as'=>'activity.model.'],function(){
Route::get('/add',[ActivityModelController::class,'create'])->name('add');
Route::post('/add',[ActivityModelController::class,'store']);
Route::get('/edit/{id}',[ActivityModelController::class,'edit'])->name('edit');
Route::post('/edit/{id}',[ActivityModelController::class,'update']);
Route::delete('/delete/{acm}',[ActivityModelController::class,'destroy'])->name('delete');
//Route::get('/list/{user}',[ActivityModelController::class,'index'])->name('activity.model.list');
Route::get('/list',[ActivityModelController::class,'index'])->name('list');
Route::post('/set-status-acm',[ActivityModelController::class,'setStatusActivityModel'])->name('set.status.acm');
});

//prefix('subactivity')->name('subactivity.')->
Route::group(['prefix'=>'subactivity','as'=>'subactivity.'],function(){
    Route::get('/add',[SubactivityController::class,'create'])->name('add');
    Route::post('/add',[SubactivityController::class,'store']);
    Route::get('/list',[SubactivityController::class,'index'])->name('list');
    Route::get('/edit/{subactivity}',[SubactivityController::class,'edit'])->name('edit');
    Route::post('/edit/{subactivity}',[SubactivityController::class,'update']);
    Route::delete('/delete/{subactivity}',[SubactivityController::class,'destroy'])->name('delete');
    Route::post('/set-status-subact/{subactivity?}',[SubactivityController::class,'setStatusSubActivity'])->name('set.status.subact');
});

Route::group(['prefix'=>'data','as'=>'data.'],function(){
    
    Route::get('/{activityModelID}/list',[DataController::class,'index'])->name('list');
    Route::get('/{activityModelID}/add',[DataController::class,'create'])->name('add');
    Route::post('/{activityModelID}/add',[DataController::class,'store'])->name('store');
    Route::get('/{activityModelID}/edit/{data}',[DataController::class,'edit'])->name('edit');
    Route::post('/{activityModelID}/edit/{data}',[DataController::class,'update']);
    //Route::delete('/delete/{data}',[DataController::class,'destroy'])->name('delete');
    
});

Route::group(['prefix'=>'data-multiple','as'=>'data.multiple.'],function(){

    Route::get('/{count}/add',[DataMultipleController::class,'create'])->name('add');
    Route::post('/{count}/add',[DataMultipleController::class,'store']);
    Route::get('/{count}/list',[DataMultipleController::class,'index'])->name('list');

});

});



Route::get('/forgot-password',function(){
    return view('pages.forgot');
})->name('forgot.password');

//Auth::route(['verify'=>true]);

Route::get('/dashboard',function(){    
    return view('layouts.master');
})->middleware('auth');


 Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify');

 //Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');