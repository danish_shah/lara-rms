$.ajaxSetup({
data:{_token:GLOBAL_VARS._token}
});

function ajax(url, dataObject = {}, data_type = "json") {
  var result;
  $.ajax({
    url: url,
    type: "POST",
    async: false,
    dataType: data_type,
    data: dataObject,
    
    success: function (response) {
      result = response;
    },
    complete:function(){
      setTimeout(function(){
        $('#windowPreloader').hide();
      },500);
    }
  });
  
  return result;
}

function array_remove_value(arr,value){
  const index = arr.indexOf(value);
  arr.splice(index,1);
  //return arr;
}

let ajaxv2 = (receivedParams) => {

  let result;

  defaultParams = {
    type:'POST',
    dataType:'json',
   // data:{formdata:null},
    async:false,
    success:(response) => {
      result = response;
    },
    complete:function(){
      setTimeout(function(){
        $('#windowPreloader').hide();
      },500);
    }
    
  }


  //$.extend(receivedParams.data,defaultParams.data);
  $.extend(defaultParams,receivedParams);

  $.ajax(defaultParams);

  return result;
}

$.fn.extend({
  removeAddClass: function (_rmv, _add) {
    return this.removeClass(_rmv).addClass(_add);
  },
  setBorder: function (property = null) {
    this.css("border", property == null ? "1px solid red" : property);
  }
});
////////////////////////////////////////////
(function ($) {
  $.fn.fillNextDD = function (RcvdParams) {

    let params = {
      ddColumn: "name",
      element: this.closest('.form-group').next().find('select'),
      firstOption: "<option value=''>Select a option</option>",
      complete: function () {

      }
    };



    $.extend(params, RcvdParams);

    this.change(function () {

      let data = {
        id: $(this).val()
      };

      let response = ajaxv2({
        url:params.url, 
        data:params.data
      });

      let select_options = params.firstOption;
      response.forEach(function (value, key) {
        select_options += `
        <option value="${value.id}">${value[params.ddColumn]}</option>`;
      })

      $(params.element).html(select_options);
      params.complete(this);

    });
    return this;
  };
}(jQuery));
