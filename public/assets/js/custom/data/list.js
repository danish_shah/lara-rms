$('.delete-acm').click((e) => {	
e.preventDefault();

swal({
  title: "Are you sure?",
  text: "Once deleted, you have to request Webmaster to recover this record!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {	
  	e.target.form.submit();
  } 
});

});