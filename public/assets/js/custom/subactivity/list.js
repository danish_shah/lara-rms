let acms_dd = $('#acms_dd_filter');
acms_dd.chosen({});	

$('.delete-subact').click((e) => {	
e.preventDefault();

swal({
  title: "Are you sure?",
  text: "Once deleted, you have to request Webmaster to recover this record!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {	
  	e.target.form.submit();
  } 
});

});
	



$('#dataTable').dataTable({
	  'columnDefs': [ {
        'targets': [3,5], /* column index */
        'orderable': false, /* true or false */
     }]
});

$('.bs-switch').change(function (element) {
	let _this = $(this);
	let _id = _this.attr('_id');

	let data = {
	status:_this.prop('checked') ? 1 : 0
	}

	let response = ajaxv2({
	url:`${GLOBAL_VARS.services.set_status_subact}/${_id}`,	
	data:data
	});

	$.toast({
    heading:'Status', 
    icon:response.icon,
    position:'top-right',
    hideAfter:5000,
    text:response.message
    })

	
});

