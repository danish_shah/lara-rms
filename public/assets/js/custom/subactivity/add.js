let acms_dd = $('#acms_dd');

acms_dd.chosen({});	

$('input[name=am_type]').change((e)=>{
let am_type = e.target.value

let am_type_rev =  am_type == 1 ? 2 : 1;

acms_dd.children('option').prop('selected',false);
acms_dd.children(`option[am_type="${am_type}"]`).show();
acms_dd.children(`option[am_type="${am_type_rev}"]`).hide();

/*acms_dd.prop('disabled',false);
acms_dd.trigger('chosen:udpated');
acms_dd.change();*/

acms_dd.prop('disabled',false).trigger('chosen:updated');

});

$('input[name=frequency_per_day]').change((e)=>{
	let frequency_per_day = e.target.value;
	let frequency_elem = $('#frequency');

	frequency_per_day==2 ?
	frequency_elem.show() :
	frequency_elem.hide()
})
