 <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubactivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subactivity', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->enum('am_type',['1','2']);
            $table->integer('activity_model_id');
            $table->string('subactivity',20);
            $table->enum('frequency_type',['1','2','3','4']);
            $table->enum('frequency_per_day',['1','2']);
            $table->integer('frequency')->nullable();
            $table->integer('sort_index')->nullable();
            $table->boolean('active')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subactivity');
    }
}
