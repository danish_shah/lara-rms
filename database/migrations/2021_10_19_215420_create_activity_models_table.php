<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_models', function (Blueprint $table) {
             $table->increments('id');
             $table->integer('user_id');
             $table->enum('am_type',['1','2']);
             $table->string('activity_model',20);
             $table->integer('sort_index')->nullable();
             $table->boolean('active')->default(0);
             $table->timestamps();
             $table->softDeletes();
             $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_models');
    }
}
