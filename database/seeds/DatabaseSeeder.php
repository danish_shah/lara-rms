<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

             DB::table('users')->insert([
            'first_name' => 'danish',
            'last_name' => 'shah',
            'email' => 'shah.danish@hotmail.com',
            'password' => Hash::make('Danish123#'),
        ]);

    }
}
