<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Rules\isValidPassword;  


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index(){
        return view('auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function store(Request $request)
    {


         $rules = [
            'first_name'=>'required|min:2|max:15',
            'last_name'=>'required|min:2|max:15',
            'email'=>'required|email|max:30|unique:users',
            //'password'=>'required|confirmed|min:6|max:15'
            'password'=> [
            'required',
            new isValidPassword()
            ],
            'password_confirmation'=>'required'
        ];

        // $messages = [
        //     'first_name.required'=>'The First Name field is required.',
        // ];

        $niceNames = [
        'first_name' => 'First Name',
        'last_name' => 'Last Name'
       // 'email'=>'Email',
      //  'password'=>'Password',
        ];  

        $request->validate($rules,[],);   

        $this->validate($request,$rules,[],$niceNames);

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        if($user->save()){
        
        auth()->attempt($request->only('email','password'));

        auth()->user()->sendEmailVerificationNotification();
        
        return redirect('dashboard');
        }

        

    }
}
