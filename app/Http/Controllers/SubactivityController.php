<?php

namespace App\Http\Controllers;

use App\Models\Subactivity;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\User;    
use App\Models\Subactivity_map;

use DB;

class SubactivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
          DB::enableQueryLog(); 
        $acms = auth()->user()->activityModels()->get();

        $subacts = auth()->user()->subactivities()->latest()->with(['activityModels'=>function($query){
            $query->where('subactivity_map.active',1);  
        }]);

        

        $subacts->when(request('subactivity'),function($query){
            $subactivity = request('subactivity');
            $query->where('subactivity','like','%'.$subactivity.'%');
        });

        $subacts->when(request('activity_model'),function($query){
            $acm_ids = request('activity_model');
            $query->where('subactivity','like','%'.request('subactivity').'%');
        });

        $subacts = $subacts->paginate(10);
        //$subacts->paginate(10);
            
    //    dd($subacts);
        //dd($subacts->toArray());
            $dr = DB::getQueryLog();

            //dd($dr);
        return view('subactivity.list',compact('acms','subacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $acms = auth()->user()->activityModels()->latest()->where('active',1)->get();
        $action_route = route('subactivity.add');
        return view('subactivity.add',compact('action_route','acms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function subactivity_validations($request,$subactivity_id=NULL){
            
        $rules = [
            'am_type'=>'required|in:1,2',
            'activity_model_id'=>'required',
            'subactivity'=>['required','min:2','max:20',
             Rule::unique('subactivity')->where(function($query) use ($request,$subactivity_id) {
                $query->where(['am_type'=>$request->am_type,'user_id'=>auth()->user()->id]);
                
                if($subactivity_id){
                    $query->where('id','!=',$subactivity_id);
                }

             })->whereNull('deleted_at')
            ],
            'frequency_type'=>'required|in:1,2,3,4',
            'frequency_per_day'=>'required|in:1,2',
            /*'frequency' => Rule::requiredIf(function () use ($request) {
            return $request->frequency_per_day==2;
            })*/
            'frequency'=>$request->frequency_per_day==2 ? 'required' : ''
        ];

       /* if($subactivity_id){
            $rules['subactivity'][] = Rule::unique('subactivity_map')->where(function($query) use ($subactivity_id){
                $query->where(['subactivity_id'=>$subactivity])
            });
        }*/

        $attributes = [
            'am_type'=>'activity model type',
            'frequency_type'=>'frequency type'
        ];

        $this->validate($request,$rules,[],$attributes);
    }
    public function store(Request $request)
    {
        $this->subactivity_validations($request);

        $save = auth()->user()->subactivities()->create($request->only('am_type','subactivity','frequency_type','frequency_per_day','frequency'));

        $subactivity_id = $save->id;

        $acm_ids = $request->activity_model_id;

        $subactivity_map_arr = array_map(function($acm_id) use ($subactivity_id) {
            
            return [
                'activity_model_id'=>$acm_id,
                'subactivity'=>$subactivity_id,
            ];

        } , $acm_ids);

        //dd($subactivity_map_arr);

       $save->subactivitiesMap()->createMany($subactivity_map_arr);



        if($save){
            return redirect()->route('subactivity.list')->with('message','Subactivity created successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subactivity  $subactivity
     * @return \Illuminate\Http\Response
     */
    public function show(Subactivity $subactivity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subactivity  $subactivity
     * @return \Illuminate\Http\Response
     */
    public function edit(Subactivity $subactivity)
    {
     $acms = auth()->user()->activityModels()->latest()->where('active',1)->get();
     $action_route = route('subactivity.edit',$subactivity); 
     $subactivity = $subactivity->where('id',$subactivity->id)->with(['activityModels'])->first();
     //$selected_acms = $subactivity->activityModels->pluck('id')->toArray();
     $selected_acms = $subactivity->subactivitiesMap()->where('active',1)->pluck('activity_model_id')->toArray();    

     return view('subactivity.add',compact('action_route','acms','subactivity','selected_acms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subactivity  $subactivity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subactivity $subactivity)
    {
        $this->subactivity_validations($request,$subactivity->id);

        $subactivity->am_type = $request->am_type;
    //    $subactivity->activity_model_id = $request->activity_model_id;
        $subactivity->subactivity = $request->subactivity;
        $subactivity->frequency_type = $request->frequency_type;
        $subactivity->frequency_per_day = $request->frequency_per_day;
        $subactivity->frequency = $request->frequency;


        $subactivity_id = $subactivity->id;

        $acm_ids = $request->activity_model_id;

        $acms_ids_exist = $subactivity->activityModels->pluck('id')->toArray();

        $acm_ids_arr = array_diff($acm_ids,$acms_ids_exist);

        $acm_ids_disable = array_diff($acms_ids_exist,$acm_ids);

        $subactivity_map_arr = array_map(function($acm_id) use ($subactivity_id) {

            return [
                'activity_model_id'=>$acm_id,
                'subactivity'=>$subactivity_id,
            ];

        } , $acm_ids_arr);  

        $subactivities_map = $subactivity->subactivitiesMap();

        $remove_ids = $subactivities_map
                                   ->whereIn('activity_model_id',$acm_ids_disable)
                                   ->get()->pluck('id')->toArray();


        $save = $subactivity->save();
        $subactivities_map->createMany($subactivity_map_arr);
        $subactivities_map->whereIn('id',$remove_ids)->update(['active'=>0]);

        if($save){
            return redirect()->route('subactivity.list')->with('message','Subactivity updated successfully.');
        }
    }   

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subactivity  $subactivity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subactivity $subactivity)
    {
        $subactivity->delete();

        return back()->with('swal_delete','Subactivity deleted successfully.');
    }

    public function setStatusSubActivity(Request $request,Subactivity $subactivity){
            $subactivity->active = $request->status;
            $subactivity->updated_by = auth()->user()->id;

            $save = $subactivity->save();

             $staus_label = $request->status == 1 ? 'actived' : 'deactived';
             $message = $save ?
                    ['icon'=>'success','message'=>$subactivity->subactivity.' '.$staus_label.' successfully.'] : 
                    ['icon'=>'error','message'=>'Error updating status'] ;

            return response()->json($message);
    }
}
