<?php

namespace App\Http\Controllers;

use App\Models\Data;
use Illuminate\Http\Request;

class DataMultipleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    
    public function validations($request){
        $rules = [
            'date'=>'required',
            "frequency"    => "array",
            'frequency.*'=>'required|array',
        ];

        $this->validate($request,$rules,[],['frequency.*'=>'frequency']);
    }

    public function index($count)
    {   
        $data = collect();
        $subacts = auth()->user()->multipleSubActivities($count)->get();
        $acm_ids = $subacts->pluck('activity_model_id');
        $subacts_ids = $subacts->pluck('subactivity_id');
        
        $data = Data::wherein('activity_model_id',$acm_ids)->with(['subactivities'=>function($query) use ($subacts_ids) {
            $query->wherein('data_map.subactivity_id',$subacts_ids);
        }])->get();

        //dd($data->toArray());
        return view('data.list_multiple',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($count)
    {
        $action_route = route('data.multiple.add',$count);
        $subacts = auth()->user()->multipleSubActivities($count)->get();

        /*//$query  = auth()->user()->multipleAcmModelsSubActivities();
        $query->when($count,function($q) use ($count) {
            return $q->having('c',$count);
        });*/

        ///dd($query->with('')->get()->toArray());  
        return view('data.add_todo_multiple',compact('action_route','subacts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $multiple_count = request()->segment(2);

        $this->validations($request);
         ///    dd($request->all());
        $date = $request->date;
        $acms = $request->activity_model_id;
        $frequencies = $request->frequency;
      //   dd($acms);
          //$found = Data::where(['activity_model_id'=>$acm,'_date'=>$date])->first();

          $fd = [];
          foreach($acms as $index=>$acm){
                                
            $found = auth()->user()->data()->where(['activity_model_id'=>$index,'_date'=>$date])->first();

                          

            if(!$found){
               foreach($acm as $subactivity_index=>$subactivity_id){
                        $fd[$index][] = ['subactivity_id'=>$subactivity_id,'frequency'=>$frequencies[$index][$subactivity_index]];     

            } 

            }
            else {

                  foreach($acm as $subactivity_index=>$subactivity_id){

                       /* $fd['found'][$found->id][] = ['subactivity_id'=>$subactivity_id,'frequency'=>$frequencies[$index][$subactivity_index]];  */
                       //$found->where('subactivity_ids',$subactivity_id);
                       
                      $subfound = $found->dataMap()->where(['subactivity_id'=>$subactivity_id])->first();
                    
                      if(!$subfound){

                        if(!isset($fd['found'][$found->id])){
                            $fd['found'][$found->id] = [];
                            $fd['found'][$found->id][] = ['subactivity_id'=>$subactivity_id,'frequency'=>$frequencies[$index][$subactivity_index]];
                        }
                      }
            } 
               
            }
       
          }
          
          foreach($fd as $key=>$value){
            if($key=='found'){
                continue;
            }

            $acm_data = ['_date'=>$date,'activity_model_id'=>$key];

            $data = auth()->user()->data()->create($acm_data);

            $data->dataMap()->createMany($value);

          }
        
        return redirect()->route('data.multiple.list',$multiple_count);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function show(Data $data)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function edit(Data $data)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Data $data)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function destroy(Data $data)
    {
        //
    }
}
