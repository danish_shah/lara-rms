<?php

namespace App\Http\Controllers;

use App\Models\ActivityModel;
use App\Models\Subactivity;
use App\Models\Data;
use Illuminate\Http\Request;


class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($acm_id)
    {   

        
        $data = Data::where('activity_model_id',$acm_id)->with('subactivities')->get();

        //dd($data->toArray());
        return view('data.list',['data'=>$data,'acm_id'=>$acm_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function validations($request){
        $rules = [
            'date'=>'required',
            'frequency.*'=>'required|numeric',
        ];

        $this->validate($request,$rules,[],['frequency.*'=>'frequency']);
    }

    public function create($activityModelID)
    {
        //die($activityModelID);
            $subacts = auth()->user()->singleSubActivities($activityModelID)->get();
          //  dd($subacts->toArray());
            /*$subacts_acm =  array_filter($subacts,function($subact) use ($activityModel) {
                if($subact['activity_model_id']==$activityModelID){
                    return $subact;
                }
            });*/
            //    dd($subacts->toArray());
        $action_route = route('data.add',$activityModelID);
        return view('data.add_todo',compact('action_route','subacts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($acm_id,Request $request)
    {
        //
        //echo '<pre>';

        $this->validations($request);

        $exist = Data::where(['activity_model_id'=>$acm_id,'_date'=>$request->date])->count();

        if($exist){
            return redirect()->route('data.list',$acm_id)->with(['message'=>'Data for the activity model on this date already exist.','class'=>'danger']);
        }

        $data = ['_date'=>$request->date,'activity_model_id'=>$acm_id];
              
        $data = auth()->user()->data()->create($data);

        $subactivity_ids = $request->subactivity_id;
        $frequencies = $request->frequency;

        $data_map = array_map(function($subactivity_id,$index) use ($frequencies) {
            
            return ['subactivity_id'=>$subactivity_id,'frequency'=>$frequencies[$index]];

        },$subactivity_ids,array_keys($subactivity_ids));
        
        $data->dataMap()->createMany($data_map);

        return redirect()->route('data.list',$acm_id)->with(['message'=>'Data added successfully.','class'=>'success']);
        
       //dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function show(Data $data)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function edit($acm_id,Data $data)
    {
        $action_route = route('data.edit',[$acm_id,$data]);
        $subacts = $data->subactivities()->get();
            //dd($subacts->toArray());

        return view('data.add_todo',compact('action_route','data','subacts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$acm_id,Data $data)
    {
        //
       // dd($request->all(),1);

        $this->validations($request);
        $exist = Data::where(['activity_model_id'=>$acm_id,'_date'=>$request->date])->where('id','!=',$data->id)->count();

        if($exist){
            return redirect()->route('data.list',$acm_id)->with(['message'=>'Trying to update the data for the date which already exist.','class'=>'danger']);
        }



        

        $data->_date = $request->date;
        $data->updated_by = auth()->user()->id;
        $data->save();

        $data_map_ids = $request->data_map_id;
        $subactivity_ids = $request->subactivity_id;
        $frequencies = $request->frequency;

        foreach($data_map_ids as $index=>$data_map_id){
            $data->dataMap()->where(['id'=>$data_map_id,'data_id'=>$data->id,'subactivity_id'=>$subactivity_ids[$index]])
                            ->update(['frequency'=>$frequencies[$index]]);
        }

        return redirect()->route('data.list',$acm_id)->with(['message'=>'Data updated successfully','class'=>'success']);
       // dd($data->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    /*public function destroy(Data $data)
    {
        $data->delete();
        return back()->with('swal_delete','Data deleted successfully.');
    }*/
}
