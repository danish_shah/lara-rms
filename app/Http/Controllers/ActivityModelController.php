<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\ActivityModel;
use App\Models\User;
use App\Models\Subactivity_map;
use DB;

class ActivityModelController extends Controller
{

    private function validate_acm($request){
        $rules = [
            'am_type'=>'required|in:1,2',
            'activity_model'=>['required','string','min:2','max:20',
            Rule::unique('activity_models')->where(function($query) use ($request) {               
                $condition = [
                    'am_type'=>$request->am_type,
                    'user_id'=>auth()->user()->id,
                    'deleted_at'=>NULL
                 ]; 
                return $query->where($condition)->where('id','!=',$request->id);

            })

            ]
        ];

        $attributes = ['am_type'=>'Actiivty Model Type'];

        $this->validate($request,$rules,[],$attributes);
    }
    public function index(){

        /*dd(Subactivity_map::selectRaw('count(subactivity_id) as sub')->groupBy('subactivity_id')
            ->having('sub',1)
            ->get());*/



        $activity_models = auth()->user()->activityModels()->get();
        return view('activity_model.list',compact('activity_models'));
    }
    public function create(){
        $action_route = route('activity.model.add');
        return view('activity_model.add',compact('action_route'));
    }

    public function store(Request $request){
        
        $this->validate_acm($request);
        $ac = auth()->user()->activityModels()->create($request->only('am_type','activity_model'));


        if($ac->save()){
        return redirect()->route('activity.model.list')->with('message','Activity model created successfully.');
        }
        
    }

    public function edit($id){
        $acm = ActivityModel::find($id);
        $action_route = route('activity.model.edit',$id);
        return view('activity_model.add',compact('acm','action_route'));
    }

    public function update(Request $request,$id){
        $this->validate_acm($request);   

        $acm = ActivityModel::find($id);

        $acm->activity_model = $request->activity_model;
        $acm->am_type = $request->am_type;
        $acm->updated_by = auth()->user()->id;
        
        if($acm->save()){
        return redirect()->route('activity.model.list')->with('message','Activity model updated successfully.');
        }
        
    }

    public function destroy(ActivityModel $acm){
        
        $acm_name = $acm->activity_model;
        
        $acm->delete();

        return back()->with('swal_delete','Activity model deleted successfully');
    }

    public function setStatusActivityModel(Request $request){
        $acm = ActivityModel::where('id',$request->_id)
                       ->update(['active'=>$request->status,'updated_by'=>auth()->user()->id]);

        $staus_label = $request->status == 1 ? 'actived' : 'deactived';
        $message = $acm ?
                    ['icon'=>'success','message'=>'Activity model '.$staus_label.' successfully.'] : 
                    ['icon'=>'error','message'=>'Error updating status'] ;

        return response()->json($message);
    }
}
