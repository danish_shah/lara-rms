<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subactivity_map extends Model
{
    protected $table = 'subactivity_map';
    
    protected $fillable = [
        'activity_model_id','subactivity_id'
    ];

    /*public function singleAcmSubactivities(){
        //SELECT DISTINCT(activity_model_id),COUNT(*) c FROM `subactivity_map` GROUP BY `subactivity_id` HAVING c=1
        return $this->select(\DB::raw('DISTINCT(activity_model_id)'), \DB::raw('COUNT(*) as c'))
                    ->groupBy('subactivity_id')
                    ->having('c', '=' , 1);


        //select('*')->groupBy('subactivity_id')->havingRaw('COUNT(*) = 1');
    }*/

    public function activityModel()
    {   
        return $this->belongsTo(ActivityModel::class);    
    }

    public function subactivity(){
        return $this->belongsTo(Subactivity::class);
    }
}
