<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $fillable = [
        '_date','activity_model_id'
    ];

    public function dataMap(){
        return $this->hasMany(Data_map::class);
    }

      public function subactivities()
    {   
        return $this->belongsToMany(Subactivity::class,Data_map::class)->withPivot(['id','frequency']);    
    }
    

    public function hasManyTh(){
        /*return $this->hasManyThrough(Data_map::class,Subactivity::class);*/
    }
}
