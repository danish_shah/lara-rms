<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Subactivity extends Model
{
    use SoftDeletes;
    
    protected $table = 'subactivity';
        //activity_model_id
    protected $fillable = [
        'am_type','subactivity','frequency_type','frequency_per_day','frequency'
    ];
    
    public function activityModels()
    {   
        return $this->belongsToMany(ActivityModel::class,Subactivity_map::class);    
    }
    
     public function subactivitiesMap()
    {   
        return $this->hasMany(Subactivity_map::class);    
    }
}
