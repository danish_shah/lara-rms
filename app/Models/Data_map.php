<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Data_map extends Model
{
    public $timestamps = FALSE;

    protected $table = 'data_map';

    protected $fillable = [
        'subactivity_id','frequency'
    ];

    public function sub(){
        return $this->belongsTo(Subactivity::class);
    }
}
