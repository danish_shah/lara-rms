<?php

namespace App\Models;
//$user->sendEmailVerificationNotification();

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\ActivityModel;
use App\Models\Subactivity;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function activityModels(){
        return $this->hasMany(ActivityModel::class);
    }

    public function subactivities(){
        return $this->hasMany(Subactivity::class);
    }

    public function data(){
        return $this->hasMany(Data::class);
    }

    /*public function datam(){
        return $this->hasManyThrough(Data_map::class,Data::class);
    }*/
    // subactivities which only belongs to one activity model
    public function singleAcmModelsSubActivities($am_type=1){
        return $this->hasManyThrough(Subactivity_map::class,Subactivity::class)
                    ->select(\DB::raw('DISTINCT(subactivity_map.activity_model_id)'), \DB::raw('COUNT(*) as c'))
                    ->where('subactivity.am_type',$am_type)
                    ->where('subactivity.active',1)
                    ->where('subactivity_map.active',1)
                    ->groupBy('subactivity_id')
                    ->having('c', '=' , 1);
                    
    }
    // subactivities which belongs to many activity models
    public function multipleAcmModelsSubActivities($am_type=1){
        return $this->hasManyThrough(Subactivity_map::class,Subactivity::class)
                    ->select(\DB::raw('COUNT(*) as c,subactivity_id'))
                    ->where('subactivity.am_type',$am_type)
                    ->where('subactivity.active',1)
                    ->where('subactivity_map.active',1)
                    ->groupBy('subactivity_id')
                    ->orderBy('c')
                    ->having('c', '>' , 1);
    }

    public function singleSubActivities($activity_model_id,$am_type=1){
        return $this->hasManyThrough(Subactivity_map::class,Subactivity::class)
                    ->select('frequency_per_day','frequency','subactivity_map.activity_model_id','subactivity_id','subactivity',\DB::raw('COUNT(*) as c'))
                    ->where('subactivity.am_type',$am_type)
                    ->where('subactivity.active',1)
                    ->where('subactivity_map.active',1)
                    ->groupBy('subactivity_map.subactivity_id')
                    ->having('c', '=' , 1)
                    ->having('activity_model_id', '=' , $activity_model_id);
                    
    }

        public function multipleSubActivities($count,$am_type=1){
        return $this->hasManyThrough(Subactivity_map::class,Subactivity::class)
                    ->select('frequency_per_day','frequency','activity_model_id','subactivity_id','subactivity',\DB::raw('COUNT(*) as c'))
                    ->where('subactivity.am_type',$am_type)
                    ->where('subactivity.active',1)
                    ->where('subactivity_map.active',1)
                    ->groupBy('subactivity_map.subactivity_id')
                    ->having('c', '=' , $count);
                    ///->having('activity_model_id', '=' , $activity_model_id);
                    
    }

    public function hasManyTh(){
        return $this->hasManyThrough(ActivityModel::class,Subactivity::class);
    }

    // create has many through for data and data map then use auth()->user()->datahasmanythrough()
}
