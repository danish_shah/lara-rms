<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ActivityModel extends Model
{
    use SoftDeletes;
    protected $fillable = [
       'am_type','activity_model' 
    ];
    
    public function ownedBy(User $user){
      return $user->id == $this->user_id;
    }

    public function subactivitiesMapSingle($acm_id){
        /*return $this->hasMany(Subactivity_map::class)
                    ->select('subactivity_id',\DB::raw('COUNT(*) as c'))
                    ->where('actives',1)
                    //->where('activity_model_id',$acm_id)
                    ->groupBy('subactivity_id')
                    ->having('c', '=' , 1);*/
    }

    public function singleAcmModelsSubActivities($am_type=1){
        /*return $this->hasManyThrough(Subactivity_map::class)->select(\DB::raw('COUNT(*) as c'))
                    ->where('subactivity.am_type',$am_type)
                    ->where('subactivity.active',1)
                    ->where('subactivity_map.active',1)
                    ->groupBy('subactivity_id')
                    ->having('c', '=' , 1);*/
                    
    }
    
}
