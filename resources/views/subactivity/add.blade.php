@extends('layouts.master')

@section('content')

@php
//dd($subactivity->toArray());
//var_dump(old('activity_model_id'));exit;    
 @endphp

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Subactivty</h1>
                    </div>

                    <div class="row">

                        <div class="col-lg-9">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Subactivty Form</h6>
                                </div>
                                <div class="card-body">
                                 <form action="{{ $action_route }}" method="post">
 <fieldset class="form-group row">
    <legend class="col-form-label col-sm-3 float-sm-left pt-0">Activity Model Type</legend>
    <div class="col-sm-9">
      <div class="form-check">
        @csrf
        <input class="form-check-input" type="radio" name="am_type" id="radio1" value="1" 
        {{ old('am_type',$subactivity->am_type ?? '')==1 ? 'checked':'' }}
        >
        <label class="form-check-label" for="radio1">
          Todo
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" name="am_type" id="radio2" value="2"
        {{ old('am_type',$subactivity->am_type ?? '')==2 ? 'checked':'' }}
        >
        <label class="form-check-label" for="radio2">
          Not Todo
        </label>
      </div>
      @error('am_type')
      <div class="invalid-feedback d-block">
      {{ $message }}
      </div>
      @enderror
    </div>
  </fieldset>
   <div class="form-group row">
    <label for="inputEmail3" class="col-sm-3 col-form-label">Actvity Models Selection</label>
    <div class="col-sm-9">
      <!-- <input type="text" name="activity_model" class="form-control @error('activity_model') is-invalid @enderror" id="inputEmail3" placeholder="Type Acitivity Model Name here..." value="{{ old('activity_model',$acm->activity_model ?? '' )}}"> -->

      <select class="form-control" id="acms_dd" {{ old('am_type',$subactivity->am_type ?? '') =='' ? 'disabled':''}} multiple name="activity_model_id[]">
          <option value=""></option>

          @foreach($acms as $key=>$acm)
          <!-- {{ old('activity_model_id'.$key,$subactivity->activity_model_id ?? '')==$acm->id ? 'selected' : '' }}  -->
          <option 

            {{ in_array($acm->id,old('activity_model_id',$selected_acms ?? [])) ? 'selected' : '' }}
            value="{{ $acm->id }}" am_type="{{ $acm->am_type }}" style="display:{{ old('am_type',$subactivity->am_type ?? '')==1 && $acm->am_type!=1 ? 'none' : (old('am_type',$subactivity->am_type ?? '')==2 && $acm->am_type!=2 ? 'none': '')  }}"> {{ $acm->activity_model }} </option>
            }
          @endforeach
      </select>

      @if($errors->has('activity_model_id'))
      <div class="invalid-feedback d-block">
        Activity Model is required
      </div>
     @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-3 col-form-label">Subactivity Name</label>
    <div class="col-sm-9">
      <input type="text" name="subactivity" class="form-control @error('subactivity') is-invalid @enderror" id="inputEmail3" placeholder="Type Subactivty Name here..." value="{{ old('subactivity',$subactivity->subactivity ?? '') }}">
      @error('subactivity')
      <div class="invalid-feedback d-block">
          {{$message}}
      </div>
     @enderror
    </div>
  </div>

   <fieldset class="form-group row">
    <legend class="col-form-label col-sm-3 float-sm-left pt-0">Frequency Type</legend>
    <div class="col-sm-9">
      <div class="form-check">
        
        <input class="form-check-input" type="radio" name="frequency_type" id="gridRadios3" value="1"
        {{ old('frequency_type',$subactivity->frequency_type ?? '')==1 ? 'checked' : '' }}
        >
        <label class="form-check-label" for="gridRadios3">
          Daily
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" name="frequency_type" id="gridRadios4" value="2"
        {{ old('frequency_type',$subactivity->frequency_type ?? '')==2 ? 'checked' : '' }}
        >
        <label class="form-check-label" for="gridRadios4">
          Weekly
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" name="frequency_type" id="radio5" value="3"
        {{ old('frequency_type',$subactivity->frequency_type ?? '')==3 ? 'checked' : '' }}
        >
        <label class="form-check-label" for="radio5">
          Monthly
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" name="frequency_type" id="radio6" value="4"
        {{ old('frequency_type',$subactivity->frequency_type ?? '')==4 ? 'checked' : '' }}
        >
        <label class="form-check-label" for="radio6">
          Custom
        </label>
      </div>
      @error('frequency_type')
      <div class="invalid-feedback d-block">
      {{ $message }}
      </div>
      @enderror
    </div>
  </fieldset>

   <fieldset class="form-group row">
    <legend class="col-form-label col-sm-3 float-sm-left pt-0">Frequency Per Day</legend>
    <div class="col-sm-9">
      <div class="form-check">
        
        <input class="form-check-input" type="radio" name="frequency_per_day" id="radio7" value="1"
        {{ old('frequency_per_day',$subactivity->frequency_per_day ?? '')==1 ? 'checked' : '' }}
        >
        <label class="form-check-label" for="radio7">
          Once
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" name="frequency_per_day" id="radio8" value="2"
        {{ old('frequency_per_day',$subactivity->frequency_per_day ?? '')==2 ? 'checked' : '' }}
        >
        <label class="form-check-label" for="radio8">
          More than once
        </label>
      </div>
      @error('frequency_per_day')
      <div class="invalid-feedback d-block">
      {{ $message }}
      </div>
      @enderror
    </div>
  </fieldset>
  
  <div class="form-group row" style="display:@if(old('frequency_per_day',$subactivity->frequency_per_day ?? '')==2)  @else none @endif;" id="frequency">
    <label for="inputEmail3" class="col-sm-3 col-form-label">Frequency</label>
    <div class="col-sm-9">
      <input type="text" name="frequency" class="form-control @error('frequency') is-invalid @enderror" id="inputEmail3" placeholder="Type Subactivty Name here..." value=" {{ old('frequency',$subactivity->frequency ?? '') }}">
      @error('frequency')
      <div class="invalid-feedback d-block">
          {{$message}}
      </div>
     @enderror
    </div>
  </div>

  <div class="form-group row">
    <label class="col-sm-3"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-primary">Submit</button>
      <a href="{{ route('subactivity.list') }}" class="btn btn-danger">Cancel</a>
    </div>
  </div>
</form>
                                </div>
                            </div>

                        </div>

                    </div>
@endsection('content')

@push('scripts')
<script src="{{ asset('assets/js/custom/subactivity/add.js') }}"></script>
@endpush  