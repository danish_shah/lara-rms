@extends('layouts.master')

@section('content')
    

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Subactivity</h1>



                    </div>

                    <div class="row">

                                 <div class="col-lg-12">
                          @if(session('message'))
                          <div class="alert alert-success"> 
                          {{ session('message') }}
                          </div>
                          @endif
                            <!-- Dropdown Card Example -->
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Subactivities List</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">ACM actions:</div>
                                            <a class="dropdown-item" href="{{ route('subactivity.add') }}">
                                            <i class="fas fa-plus"></i>
                                            Add New
                                            </a>
                                            <a class="dropdown-item" href="#">
                                            <i class="fas fa-sort"></i>
                                            Set sorting
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">
                                            <i class="fas fa-times"></i>
                                            Close
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                      <div class="table-responsive">
                                <table class="table table-bordered" id="" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Serial #</th>
                                            <th>
                                            <form action="{{ route('subactivity.list') }}" method="get">

                                                <label>Activity Models</label>
                                                <select name="activity_model" class="form-control" id="acms_dd_filter" multiple>
                                                    @foreach($acms as $acm)
                                                    <option value="{{ $acm->id }}">{{ $acm->activity_model }}</option>
                                                    @endforeach
                                                </select>
                                                
                                                </form>
                                            </th>
                                            <th>    
                                                <form action="{{ route('subactivity.list') }}" method="get">
                                                <label>Subactivity</label>
                                                <input type="text" name="subactivity" class="form-control" />
                                                </form>
                                            </th>
                                            <th>AM Type</th>
                                            <th>Status</th>
                                            <th>Created at</th>
                                            <th>Actions</th>
                                        </tr> 
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Serial #</th>
                                             <th>Activity Models</th>
                                             <th>
                                                 <input type="" name="">
                                             </th>
                                            <th>AM Type</th>
                                            <th>Status</th>
                                            <th>Created at</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                      <tbody>
                                          @foreach($subacts as $index=>$subact)
                                          <tr>
                                          <td>{{ $index + $subacts->firstItem() }}</td>
                                          <td>
                                              @foreach($subact->activityModels as $acm)
                                              <span class="badge badge-primary"> {{ $acm->activity_model }} </span>
                                              @endforeach
                                          </td>
                                          <td>
                                              {{ $subact->subactivity }}
                                          </td>
                                          <td>
                                            {!! $subact->am_type==1 ? '<span class="badge badge-success">Todo</span>' : '<span class="badge badge-danger">Not Todo</span>'  !!}
                                          </td>
                                          <td>
                                            <input type="checkbox" class="bs-switch" _id="{{ $subact->id }}" data-onstyle="success" data-toggle="toggle" data-on="Active" data-off="Deactive" data-size="xs" {{ $subact->active==1?'checked':'' }} >
                                          </td>
                                          <td>
                                            {{ date('M-d h:i:s A',strtotime($subact->created_at)) }}
                                          </td>
                                          <td>  
                                            <a href="{{ route('subactivity.edit',$subact) }}" class='btn btn-primary'>
                                              <i class="fas fa-edit"></i>
                                              Edit
                                            </a>
                                            <form method="post" action="{{ route('subactivity.delete',$subact)}}" style="display: inline;">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class='btn btn-danger delete-subact'>
                                              <i class="fas fa-trash"></i>
                                              Delete
                                            </button>
                                            </form>
                                          </td>
                                          </tr>
                                          @endforeach

                                          @if(!$subacts->count())
                                          <tr>
                                              <td colspan="7">
                                                <center>
                                                  No data found
                                                  </center>
                                              </td>
                                          </tr>
                                          @endif
                                      </tbody>
                                </table>
                                {{ $subacts->appends(request()->query())->links() }}
                                
                            </div>
                                </div>
                            </div>

                        </div>

                    </div>

              

@endsection('content')

@push('scripts')
<script src="{{ asset('assets/js/custom/subactivity/list.js') }}"></script>
    <script type="text/javascript">
        GLOBAL_VARS.services['set_status_subact'] = '{{ route('subactivity.set.status.subact') }}'
         @if(session('swal_delete'))
          swal('{{ @session('swal_delete') }}', {
          icon: "success",
          });
         @endif
    </script>   
@endpush
