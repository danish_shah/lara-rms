@extends('layouts.master')

@section('content')
    

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Acitivity Model</h1>



                    </div>

                    <div class="row">

                                 <div class="col-lg-12">
                                       @if(session('message'))
                          <div class="alert alert-success"> 
                          {{ session('message') }}
                          </div>
                          @endif
                            <!-- Dropdown Card Example -->
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Activit Models List</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">ACM actions:</div>
                                            <a class="dropdown-item" href="{{ route('activity.model.add') }}">
                                            <i class="fas fa-plus"></i>
                                            Add New
                                            </a>
                                            <a class="dropdown-item" href="#">
                                            <i class="fas fa-sort"></i>
                                            Set sorting
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">
                                            <i class="fas fa-times"></i>
                                            Close
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                      <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Serial #</th>
                                            <th>Name</th>
                                            <th>AM Type</th>
                                            <th>Status</th>
                                            <th>Created at</th>
                                            <th>Actions</th>
                                        </tr> 
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Serial #</th>
                                            <th>Name</th>
                                            <th>AM Type</th>
                                            <th>Status</th>
                                            <th>Created at</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                      <tbody>
                                          @foreach($activity_models as $key=>$ac)
                                          <tr>
                                          <td>{{ ++$key }}</td>
                                          <td>{{ $ac->activity_model }}</td>
                                          <td>
                                            {!! $ac->am_type==1 ? '<span class="badge badge-success">Todo</span>' : '<span class="badge badge-danger">Not Todo</span>'  !!}
                                          </td>
                                          <td>
                                            <input type="checkbox" class="bs-switch" _id="{{ $ac->id }}" data-onstyle="success" data-toggle="toggle" data-on="Active" data-off="Deactive" data-size="xs" @php echo $ac->active==1?'checked':''; @endphp >
                                          </td>
                                          <td>
                                            {{ date('M-d h:i:s A',strtotime($ac->created_at))  }}
                                          </td>
                                          <td>  
                                            <a href="{{ route('activity.model.edit',$ac->id) }}" class='btn btn-primary'>
                                              <i class="fas fa-edit"></i>
                                              Edit
                                            </a>
                                            <form method="post" action="{{ route('activity.model.delete',$ac)}}" style="display: inline;">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class='btn btn-danger delete-acm'>
                                              <i class="fas fa-trash"></i>
                                              Delete
                                            </button>
                                            </form>
                                          </td>
                                          </tr>
                                          @endforeach
                                      </tbody>
                                </table>
                            </div>
                                </div>
                            </div>

                        </div>

                    </div>

              

@endsection('content')

@push('scripts')
<script src="{{ asset('assets/js/custom/activity_model/list.js') }}"></script>
    <script type="text/javascript">
        GLOBAL_VARS.services['set_status_acm'] = '{{ route('activity.model.set.status.acm') }}'
         @if(session('swal_delete'))
          swal('{{ @session('swal_delete') }}', {
          icon: "success",
          });
         @endif
    </script>
@endpush
