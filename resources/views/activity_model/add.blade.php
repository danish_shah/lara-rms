@extends('layouts.master')

@section('content')
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Acitivity Model</h1>
                    </div>

                    <div class="row">

                        <div class="col-lg-9">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Activity Model Form</h6>
                                </div>
                                <div class="card-body">
                                 <form action="{{ $action_route }}" method="post">
 <fieldset class="form-group row">
    <legend class="col-form-label col-sm-3 float-sm-left pt-0">Acitivity Model Type</legend>
    <div class="col-sm-9">
      <div class="form-check">
        @csrf
        <input class="form-check-input" type="radio" name="am_type" id="gridRadios1" value="1" {{ old('am_type',$acm->am_type ?? '')==1?'checked':'' }}>
        <label class="form-check-label" for="gridRadios1">
          Todo
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" name="am_type" id="gridRadios2" value="2" {{ old("am_type",$acm->am_type ?? '')==2?"checked":"" }}>
        <label class="form-check-label" for="gridRadios2">
          Not Todo
        </label>
      </div>
      @error('am_type')
      <div class="invalid-feedback d-block">
      {{ $message }}
      </div>
      @enderror
    </div>
  </fieldset>
  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-3 col-form-label">Acitivity Model Name</label>
    <div class="col-sm-9">
      <input type="text" name="activity_model" class="form-control @error('activity_model') is-invalid @enderror" id="inputEmail3" placeholder="Type Acitivity Model Name here..." value="{{ old('activity_model',$acm->activity_model ?? '' )}}">
      @error('activity_model')
      <div class="invalid-feedback d-block">
          {{$message}}
      </div>
     @enderror
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-primary">Submit</button>
      <button type="submit" class="btn btn-danger">Cancel</button>
    </div>
  </div>
</form>
                                </div>
                            </div>

                        </div>

                    </div>
@endsection('content')