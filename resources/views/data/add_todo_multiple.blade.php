@extends('layouts.master')

@section('content')
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Acitivity Model</h1>
                    </div>

                    <div class="row">

                        <div class="col-lg-9">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Activity Model Form</h6>
                                </div>
                                <div class="card-body">
                                 <form action="{{ $action_route }}" method="post">
  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-3 col-form-label">Date</label>
    <div class="col-sm-9">
      @csrf
      <input type="date" name="date" value="{{ old('date',isset($data) ? $data->_date : '') }}" class="form-control @error('date') is-invalid @enderror" id="inputEmail3" placeholder="Select a date..." value="">
      @error('date')
      <div class="invalid-feedback d-block">
          {{$message}}
      </div>
     @enderror
    </div>
  </div>

   <!--  @foreach($subacts as $index=>$subact)
    <div class="form-group row">
    <label for="inputEmail3" class="col-sm-3 col-form-label">{{ $subact['subactivity'] }}</label>
    <div class="col-sm-9">
      <input type="hidden" name="subactivity_id[]" value="{{ $subact['subactivity_id'] }}">
      @php $repeat = $subact['frequency_per_day']==1; @endphp
      <select class="form-control {{ !$repeat ? 'chosen' : '' }} {{$errors->has('frequency.'.$index) ? 'is-invalid' : '' }}"  name="frequency[]">
      <option value="">---Select----</option>
      @if($repeat)
     <option value="1" {{ old('frequency.'.$index)==1 ? 'selected' : '' }}>Yes</option>
     <option value="0" {{ old('frequency.'.$index)==0 ? 'selected' : '' }}>No</option>
      @else
      <option value="0">0</option>
      @for($i=1;$i<=$subact['frequency'];$i++)
      <option value="{{ $i }}">{{ $i }}</option>
      @endfor
      
      @endif

      </select>
      
      @if($errors->has('frequency.'.$index))
      <div class="invalid-feedback d-block">
          {{$errors->first('frequency.'.$index)}}
      </div>
     @endif
    </div>
  </div>
  @endforeach -->

 @foreach($subacts as $index=>$subact)
   @php
  // dd($subact->pivot->frequency,1);
  //dd($subacts->toArray());
  //var_dump(old('frequency.'.$index));


 // $test = isset($subact->pivot->frequency) ? $subact->pivot->frequency : '';

   @endphp
    <div class="form-group row">
    <label for="inputEmail3" class="col-sm-3 col-form-label">{{ $subact['subactivity'] }}</label>
    <div class="col-sm-9">
      @if(isset($subact->pivot->id))
      <input type="hidden" name="data_map_id[]" value="{{ $subact->pivot->id }} ">
      @endif
      <input type="hidden" name="activity_model_id[{{ $subact->activity_model_id }}][]" value="{{ isset($subact['subactivity_id']) ? $subact['subactivity_id'] : $subact->id }}">
      @php $repeat = $subact['frequency_per_day']==1; @endphp
      <select class="form-control {{ !$repeat ? 'chosen' : '' }} {{$errors->has('frequency.'.$index) ? 'is-invalid' : '' }}"  name="frequency[{{ $subact->activity_model_id }}][]">
      <option value="">---Select----</option>
      @if($repeat)
     <option value="1" {{ old('frequency.'.$index,isset($subact->pivot->frequency) ? $subact->pivot->frequency : '')==1 ? 'selected' : '' }}>Yes</option>
     <option value="0" {{ old('frequency.'.$index,isset($subact->pivot->frequency) ? $subact->pivot->frequency : '')==0 ? 'selected' : '' }}>No</option>
      @else
      <option value="0">0</option>
      @for($i=1;$i<=$subact['frequency'];$i++)
      <option value="{{ $i }}" {{ old('frequency.'.$index,isset($subact->pivot->frequency) ? $subact->pivot->frequency : '')==$i ? 'selected':'' }}>{{ $i }}</option>
      @endfor
      
      @endif

      </select>
      
      @if($errors->has('frequency.'.$index))
      <div class="invalid-feedback d-block">
          {{$errors->first('frequency.'.$index)}}
      </div>
     @endif
    </div>
  </div>
  @endforeach 

  <div class="form-group row">
    <label class="col-sm-3"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-primary">Submit</button>
      <button type="submit" class="btn btn-danger">Cancel</button>
    </div>
  </div>
</form>
                                </div>
                            </div>

                        </div>

                    </div>
@endsection('content')
@push('scripts')
<script src="{{ asset('assets/js/custom/data/add.js') }}"></script>
@endpush