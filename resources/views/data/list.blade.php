@extends('layouts.master')

@section('content')
    

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Data TODO</h1>



                    </div>

                    <div class="row">

                           <div class="col-lg-12">
                          @if(session('message'))
                          <div class="alert alert-{{session('class')}}"> 
                          {{ session('message') }}
                          </div>
                          @endif
                            <!-- Dropdown Card Example -->
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Activit Models List</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">ACM actions:</div>
                                            <a class="dropdown-item" href="{{ route('activity.model.add') }}">
                                            <i class="fas fa-plus"></i>
                                            Add New
                                            </a>
                                            <a class="dropdown-item" href="#">
                                            <i class="fas fa-sort"></i>
                                            Set sorting
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">
                                            <i class="fas fa-times"></i>
                                            Close
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                      <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Serial #</th>
                                            <th>Date</th>
                                            <th>Data</th>
                                            <th>Created at</th>
                                            <th>Actions</th>
                                        </tr> 
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Serial #</th>
                                            <th>Date</th>
                                            <th>Data</th>
                                            <th>Created at</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                      <tbody>
                                          @foreach($data as $key=>$dt)

                                          @php

                                         //dd($dt->toArray());
                                          @endphp
                                          <tr>
                                          <td>{{ ++$key }}</td>
                                          <td>{{ $dt->_date }}</td>
                                          <td>
                                            <ul>
                                             @foreach($dt->subactivities as $subacts)

                                             <li>{{ $subacts->subactivity }} : 

                                                @if($subacts->frequency_per_day==1)
                                                <span class="badge badge-{{$subacts->pivot->frequency?'success':'danger'}}">{{ $subacts->pivot->frequency ? 'Yes' : 'No' }}</span>
                                                @else
                                                <span class="badge badge-info">{{ $subacts->pivot->frequency }}</span> 
                                                @endif
                                                
                                                </li>
                                             @endforeach
                                             </ul>
                                          </td>
                                          <td>
                                            {{ date('M-d h:i:s A',strtotime($dt->created_at))  }}
                                          </td>
                                          <td>  
                                            <a href="{{ route('data.edit',[$acm_id,$dt->id]) }}" class='btn btn-primary'>
                                              <i class="fas fa-edit"></i>
                                              Edit
                                            </a>
                                          </td>
                                          </tr>
                                          @endforeach

                                          @if(!$data->count())
                                          <tr>
                                              <td colspan="5">
                                                <center>
                                                  No data found
                                                  </center>
                                              </td>
                                          </tr>
                                          @endif
                                      </tbody>
                                </table>
                            </div>
                                </div>
                            </div>

                        </div>

                    </div>

              

@endsection('content')

@push('scripts')
<script src="{{ asset('assets/js/custom/data/list.js') }}"></script>  
 <script type="text/javascript">

/*@if(session('swal_delete'))
swal('{{ @session('swal_delete') }}', {
icon: "success",
});
@endif
*/
</script>
@endpush
