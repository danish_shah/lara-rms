@php
//$inst = new \App\Models\Subactivity_map;
//$acms = $inst->singleAcmSubactivities()->with(['activityModels'])->get();
$acms_todo = auth()->user()->singleAcmModelsSubActivities()->with(['activityModel'=>function($query){
            $query->where(['activity_models.am_type'=>1,'active'=>1]);  
        }])->get();

$acms_not_todo = auth()->user()->singleAcmModelsSubActivities(2)->with(['activityModel'=>function($query){
    $query->where(['activity_models.am_type'=>2,'active'=>1]);  
}])->get(); 
$multiple_acms = auth()->user()->multipleAcmModelsSubActivities()->get();

//dd($multiple_acms->toArray());
$multiple_acms_not_todo = auth()->user()->multipleAcmModelsSubActivities(2)->get();
@endphp

  <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="index.html">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Interface
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Activity Model</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!-- <h6 class="collapse-header">Custom Components:</h6> -->
                        <a class="collapse-item" href="{{ route('activity.model.add') }}">Add</a>
                        <!-- <a class="collapse-item" href="{{ route('activity.model.list',auth()->user()) }}">List</a> -->
                        <a class="collapse-item" href="{{ route('activity.model.list') }}">List</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Subactivity</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!-- <h6 class="collapse-header">Custom Utilities:</h6> -->
                        <a class="collapse-item" href="{{ route('subactivity.add') }}">Add</a>
                        <a class="collapse-item" href="{{ route('subactivity.list') }}">List</a>
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#testtt"
                    aria-expanded="true" aria-controls="testtt">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Menu</span>
                </a>
                <div id="testtt" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!-- <h6 class="collapse-header">Custom Utilities:</h6> -->
                        <a class="collapse-item" href="{{ route('subactivity.add') }}">Add</a>
                        <a class="collapse-item" href="{{ route('subactivity.list') }}">List</a>
                    </div>
                </div>
            </li>
             <hr class="sidebar-divider">
               <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseData"
                    aria-expanded="true" aria-controls="collapseData">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Data Todo Single</span>
                </a>
                <div id="collapseData" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!-- <h6 class="collapse-header">Custom Utilities:</h6> -->
                        <!-- <a class="collapse-item" href="{{ route('subactivity.add') }}">Add</a> -->
                        @foreach($acms_todo as $acm)
                        @if(!empty($acm->activityModel['activity_model']))
                     <!--    <a class="collapse-item" href="{{ route('data.add',$acm->activityModel['id']) }}">{{ $acm->activityModel['activity_model'] }} </a> -->

                        <ul style="list-style: none;padding-left: 26px;color:#3a3b45">
                            <li>{{ $acm->activityModel['activity_model'] }}</li>
                            <ul style="list-style:none;padding-left: 11px;">
                                <li><a href="{{ route('data.add',$acm->activityModel['id']) }}" style="text-decoration: none;color:#3a3b45"><i class="fas fa-plus"></i> Add</a></li>
                                <li><a href="{{ route('data.list',$acm->activityModel['id']) }}" style="text-decoration: none;color:#3a3b45"><i class="fas fa-list"></i> List</a></li>
                            </ul>
                        </ul>
                        <hr>
                        @endif
                        @endforeach
                    </div>
                </div>
            </li>

          <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseData3"
                    aria-expanded="true" aria-controls="collapseData">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Data Todo Multiple</span>
                </a>
                <div id="collapseData3" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!-- <h6 class="collapse-header">Custom Utilities:</h6> -->
                        <!-- <a class="collapse-item" href="{{ route('subactivity.add') }}">Add</a> -->
                        @foreach($multiple_acms->unique('c') as $count)
                        
                        <a class="collapse-item" href="">Common {{ $count->c }} </a>

                        <ul style="list-style: none;padding-left: 26px;color:#3a3b45">
                            
                            <ul style="list-style:none;padding-left: 11px;">
                                <li><a href="{{ route('data.multiple.add',$count->c) }}" style="text-decoration: none;color:#3a3b45"><i class="fas fa-plus"></i> Add</a></li>
                                <li><a href="{{ route('data.multiple.list',$count->c) }}" style="text-decoration: none;color:#3a3b45"><i class="fas fa-list"></i> List</a></li>
                            </ul>
                        </ul>
                        <hr>
                        
                        @endforeach
                    </div>
                </div>
            </li>
             <hr class="sidebar-divider">
                <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseData2"
                    aria-expanded="true" aria-controls="collapseData">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Data Not Todo</span>
                </a>
                <div id="collapseData2" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!-- <h6 class="collapse-header">Custom Utilities:</h6> -->
                        <!-- <a class="collapse-item" href="{{ route('subactivity.add') }}">Add</a> -->
                        @foreach($acms_not_todo as $acm)
                        @if(!empty($acm->activityModel['activity_model']))
                        <a class="collapse-item" href="{{ route('subactivity.list',$acm->activityModel['id']) }}">{{ $acm->activityModel['activity_model'] }} </a>

                        @endif
                        @endforeach
                    </div>
                </div>
            </li>


          <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#cd4"
                    aria-expanded="true" aria-controls="collapseData">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Data Not Todo Multiple</span>
                </a>
                <div id="cd4" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!-- <h6 class="collapse-header">Custom Utilities:</h6> -->
                        <!-- <a class="collapse-item" href="{{ route('subactivity.add') }}">Add</a> -->
                        @foreach($multiple_acms_not_todo->unique('c') as $count)
                        
                        <a class="collapse-item" href="{{ route('subactivity.list',$acm->activityModel['id']) }}">Common {{ $count->c }} </a>
                        
                        @endforeach
                    </div>
                </div>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Addons
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                    aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Pages</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Login Screens:</h6>
                        <a class="collapse-item" href="login.html">Login</a>
                        <a class="collapse-item" href="register.html">Register</a>
                        <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Other Pages:</h6>
                        <a class="collapse-item" href="404.html">404 Page</a>
                        <a class="collapse-item" href="blank.html">Blank Page</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Charts -->
            <li class="nav-item active">
                <a class="nav-link" href="charts.html">
                    <i class="fas fa-fw fa-chart-area"></i>
                    <span>Charts</span></a>
            </li>

            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="tables.html">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Tables</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>